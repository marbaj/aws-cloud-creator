"use strict";
const fs = require("fs");
const cloud = require("./lib/cloud-formation");
const config = require("./config");

const opsworks = require("./config/ops-works");
const netformer = require("./config/network-former");

const opts = Object.assign({ stack: opsworks }, netformer);

const template = cloud(opts);

fs.writeFileSync('./template.json', JSON.stringify(template, null, 2), 'utf-8'); 