"use strict";

const opsWorks = require('./ops-works.json' );
const network = require ('./network-former.json');
const lists = ["securityGroups", "routeTables", "subnets", "eips", "elbs"];

var setup = { };

lists.forEach(l => {
    setup[l] = (network[l] || []).concat((opsWorks[l] || []));
    delete network[l];
    delete opsWorks[l];
});

module.exports = Object.assign(setup, network, { opsWorks: opsWorks });
