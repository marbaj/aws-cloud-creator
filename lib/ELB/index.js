"use strict";

const elbCreator = require("./elb");
const config = require("../../config");

module.exports = () => {
    if (!config.elbs) {
        return { };
    }

    var elbs = { };

    config.elbs.forEach(elb => {  
        elbs = Object.assign(elbs, elbCreator(elb));
    }); 

    return elbs;
};
