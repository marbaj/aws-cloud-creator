"use strict";

const utils = require("../utils");
const config = require("../../config");

const createElb = elb => {
    const template = {
        Type: "AWS::ElasticLoadBalancing::LoadBalancer",
        Properties: {
            Listeners: [{
                "LoadBalancerPort": "80",
                "InstancePort": "80",
                "Protocol": "HTTP",
                "InstanceProtocol": "HTTP"
            }],
            HealthCheck: {
                "Target": "HTTP:80/",
                "HealthyThreshold": "2",
                "UnhealthyThreshold": "10",
                "Interval": "90",
                "Timeout": "60"
            },
            Tags: []
        }
    };

    utils.defaultTags(template.Properties.Tags, elb.name);

  //  template.Properties.CrossZone = true;
    template.Properties.SecurityGroups = (elb.securityGroups || []).map(sg => utils.ref(sg));
    template.Properties.Subnets = (elb.subnets || []).map(subnet => utils.ref(subnet));
    template.DependsOn = [config.internetGateway.name];

    const attachment = {
        "Type": "AWS::OpsWorks::ElasticLoadBalancerAttachment",
        "Properties": { }
    };

    attachment.Properties.ElasticLoadBalancerName = utils.ref(elb.name);

    if (elb.layerName) {
        attachment.Properties.LayerId = utils.ref(elb.layerName);
    }
    
    return { 
        [elb.name]: Object.assign(template, utils.metadata.generate()), 
        [`${elb.name}Attachment`]: Object.assign(attachment, utils.metadata.generate()) 
    };
};

module.exports = (elb) => {
    return createElb(elb);
};
