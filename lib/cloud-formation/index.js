"use strict";

const resources = require("./resources");
const parameters = require("./parameters");
const metadate = require("./metadata");
const mappings = require("./mappings");

module.exports = opts => {
    const cloud = {
        AWSTemplateFormatVersion: "2010-09-09",
        Parameters: { },
        Resources: { },
        Metadata: { },
        Mappings: { }
    };

    cloud.Parameters = parameters(opts);
    cloud.Resources = resources(opts);
    cloud.Metadata = metadate(opts);
    cloud.Mappings = mappings(opts);
    
    return cloud;
}