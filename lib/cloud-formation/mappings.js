"use strict";

const mappings = require("../../config/mappings");

module.exports = () => {
    return mappings;
}