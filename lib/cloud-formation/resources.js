"use strict"

const networking = require("../networking"),
    opsworks = require("../opsworks");

module.exports = opts => {
    const netObj = networking(opts);
    const opsworksObj = opsworks(opts);
   
    return Object.assign(netObj, opsworksObj);
};