"use strict";

const utils = require("../utils");
const config = require("../../config");

module.exports = () => {
    const eips = { };
     
    config.eips.forEach(eip => {
        const template = {
            "Type" : "AWS::EC2::EIP",
            "Properties" : { }
        };

        template.Properties.Domain = "vpc";
        template.Properties.InstanceId = eip.instanceName ? utils.ref(eip.instanceName) : undefined;
        
        eips[eip.name] = Object.assign(template, utils.metadata.generate());
    });

    return eips;
};