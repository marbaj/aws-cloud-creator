"use strict";

const utils = require("../utils");
const config = require("../../config");

module.exports = () => {
    const subnets = { };

    config.subnets.forEach(subnet => {
        const template = {
            "Type": "AWS::EC2::Subnet",
            "Properties": {
                Tags: []
            }
        };

        template.Properties.Tags = utils.defaultTags(subnet.name);

        template.Properties.VpcId = utils.ref(config.vpc.name);
        template.Properties.AvailabilityZone = subnet.az;
        template.Properties.CidrBlock = subnet.CidrBlock || { "Fn::FindInMap" : [ "SubnetConfig", subnet.name, "CIDR" ] };
        template.Properties.MapPublicIpOnLaunch = subnet.publicIp || false;

        subnets[subnet.name] = Object.assign(template, utils.metadata.generate());
        if (subnet.routeTable) {
            let attachment = {
                Type : "AWS::EC2::SubnetRouteTableAssociation",
                Properties : { }
            };

            let name = subnet.routeTable;
            attachment.Properties.RouteTableId = utils.ref(name);
            attachment.Properties.SubnetId = utils.ref(subnet.name);

            subnets[`${subnet.name}${name}`] = attachment;
        }
    });

    return subnets;
};
