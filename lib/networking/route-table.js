
"use strict";

const utils = require("../utils");
const config = require("../../config");

const createTable = table => {

    const template = {
        "Type": "AWS::EC2::RouteTable",
        "Properties" : {  
            Tags: []
        }
    }; 

    template.Properties.Tags = utils.defaultTags(table.name);

    template.Properties.VpcId = utils.ref(config.vpc.name);

    const routeTable = { [table.name]: Object.assign(template, utils.metadata.generate()) };

    // add routs to tour table
    table.routes.forEach((route, i) => {
        const obj = {
            "Type" : "AWS::EC2::Route",
            "Properties" : { }
        };

        obj.Properties.DestinationCidrBlock = route.destination;

        switch (route.type) {
            case "internet":
                obj.Properties.GatewayId = utils.ref(config.internetGateway.name);
                obj.DependsOn = "VPCGatewayAttachment";
                break;
            case "nat":
                obj.Properties.NatGatewayId = utils.ref(config.natGateway.name);
                break;
        }

        obj.Properties.RouteTableId = utils.ref(table.name);
        
        routeTable[`${table.name}route${i}`] = obj;
    });

    return routeTable;
};

module.exports = opts => {
    var routeTables = { };

    config.routeTables.forEach(routeTable => {
        routeTables = Object.assign(routeTables, createTable(routeTable));
    });

    return routeTables;
};
