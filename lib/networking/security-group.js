"use strict";

const utils = require("../utils");
const config = require("../../config");

const createSG = securityGroup => {
    const template = {
        "Type": "AWS::EC2::SecurityGroup",
        "Properties": { 
            SecurityGroupIngress: [],
            Tags: []
        }
    };

    template.Properties.Tags = utils.defaultTags(securityGroup.name);

    template.Properties.GroupDescription = securityGroup.description;
    template.Properties.VpcId = utils.ref(config.vpc.name);
    template.Properties.SecurityGroupIngress = securityGroup.ingress || [];

    var ingress = securityGroup.ingress || [];
    
    template.Properties.SecurityGroupIngress = securityGroup.ingress.map(ing => {
        const obj = {
            FromPort: ing.fromPort || ing.FromPort,
            ToPort: ing.toPort || ing.ToPort,
            IpProtocol: ing.protocol || ing.IpProtocol
        };

        obj.CidrIp = utils.getCidrIp(ing, "subnets", ing.subnet);

        return obj;
    });
    
    return  { [securityGroup.name]: Object.assign(template, utils.metadata.generate()) }
}

module.exports = () => {
    var securityGroups = { };
    config.securityGroups.forEach(sg => {
        securityGroups = Object.assign(securityGroups, createSG(sg))
    });

    return securityGroups;
}
