"use strict";

const eips = require("./eip"),
    securityGroup = require("./security-group"),
    vpc = require("./vpc"),
    subnets = require("./subnet"),
    gateway = require("./internet-gateway"),
    routeTable = require("./route-table"),
    natGateway = require("./nat-gateway"),
    instances = require("../instances");

module.exports = () => {
    const natGatewayObj = natGateway();
    const eipsObj = eips();
    const securityGroupObj = securityGroup();
    const vpcObj = vpc();
    const subnetsObj = subnets();
    const gatewayObj = gateway();
    const routeTableObj = routeTable();
    const instancesObj = instances();

    return Object.assign(
        natGatewayObj,
        eipsObj,  
        securityGroupObj, 
        vpcObj, 
        subnetsObj, 
        gatewayObj, 
        routeTableObj,
        instancesObj);
};
