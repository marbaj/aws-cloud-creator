"use strict";

const utils = require("../utils");
const config = require("../../config");

module.exports = () => {
    const template = {
        Type: "AWS::EC2::VPC",
        Properties: {
            CidrBlock: { "Fn::FindInMap" : [ "SubnetConfig", config.vpc.name, "CIDR" ] },
            EnableDnsSupport: "true",
            EnableDnsHostnames: "true",
            InstanceTenancy: "default",
            Tags: []
        }
    };

    template.Properties.Tags = utils.defaultTags(config.vpc.name);

    return { [config.vpc.name]: Object.assign(template, utils.metadata.generate()) };
};
