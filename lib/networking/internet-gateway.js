"use strict";

const utils = require("../utils");
const config = require("../../config");

module.exports = () => {
    const template = {
        "Type" : "AWS::EC2::InternetGateway",
        "Properties" : { }
    };

    const attachment = {
        "Type" : "AWS::EC2::VPCGatewayAttachment",
        "Properties" : { }
    };

    attachment.Properties.InternetGatewayId = utils.ref(config.internetGateway.name);
    attachment.Properties.VpcId = utils.ref(config.vpc.name);

    return { 
        [config.internetGateway.name]: Object.assign(template, utils.metadata.generate()),
        VPCGatewayAttachment: Object.assign(attachment, utils.metadata.generate()) 
    };
};