"use strict";

const utils = require("../utils");
const config = require("../../config");

module.exports = () => {
    if (!config.natGateway) {
        return { };
    }

    const template = {
        "Type" : "AWS::EC2::NatGateway",
        "Properties" : { }
    };

    template.Properties.SubnetId = utils.ref(config.natGateway.subnet);
    template.Properties.AllocationId = { "Fn::GetAtt": [config.natGateway.eip, "AllocationId"] };

    return { [config.natGateway.name]: Object.assign(template, utils.metadata.generate()) };
};