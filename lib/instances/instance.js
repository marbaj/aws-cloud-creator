"use strict";

const utils = require("../utils");

module.exports = (instance, vpcName) => {

    const template = {
        Type : "AWS::EC2::Instance",
        Properties : {
            ImageId : { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "AMI" ]},
            Tags: []
        }
    };

    template.Properties.Tags = utils.defaultTags(instance.name);

    template.Properties.KeyName = instance.sshKeyName;
    template.Properties.SubnetId = utils.ref(instance.subnet);   
    template.Properties.SecurityGroupIds = (instance.securityGroups || []).map(sg => utils.ref(sg));
      
    return { [instance.name]: Object.assign(template, utils.metadata.generate()) }
};
