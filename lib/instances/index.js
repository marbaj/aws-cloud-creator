"use strict";

const utils = require("../utils");
const ec2 = require("./instance");
const config = require("../../config");

module.exports = () => {
    if (!config.instances) {
        return { };
    }

    var instances = { };

    config.instances.forEach(inst => {  
        instances = Object.assign(instances, ec2(inst, config.vpc));
    }); 

    return instances;
}