"use strict";

const uuidV1 = require('uuid/v1');
const config = require("../config");

const metadata = {
    generate: function () {
        const metadata = {
            Metadata: {
                "AWS::CloudFormation::Designer": {
                    "id": uuidV1()
                }
            }
        }

        return metadata; 
    },
    getId: function (metadata) {
        return metadata["AWS::CloudFormation::Designer"].id;
    }
};

const ref = function (refName) {
    return { Ref: refName }
};

const defaultTags = function (name) {
    var defTags =  [{
        Key: "Name",
        Value: name
    }];

    return defTags;
};

const getCidrIp = function (obj, section, name) {
    var CidrIp = '';
    if (obj['CidrIp']) {
        CidrIp = obj['CidrIp'];
    } else {
        let target = (config[section] || []);
        (Array.isArray(target) ? target : [target]).forEach(t => {
            CidrIp = t.name === name ? t.CidrBlock : CidrIp;
        });
    }

    return CidrIp;
};

module.exports = {
    ref: ref,
    metadata: metadata,
    defaultTags: defaultTags,
    getCidrIp: getCidrIp
};
