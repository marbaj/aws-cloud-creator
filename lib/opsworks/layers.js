"use strict";

const utils = require("../utils");
const ec2 = require("./instance");
const config = require("../../config");

const createLayer = (layer, stackName) => {
    const template = {
        Type: "AWS::OpsWorks::Layer",
        Properties: {
            Type: "custom",
            Shortname: "front-app",
            EnableAutoHealing: "true",
            AutoAssignElasticIps: "false",
            AutoAssignPublicIps: "false",
            CustomSecurityGroupIds: []
        }
    };

    template.Properties.StackId = utils.ref(stackName);
    template.Properties.Name = layer.name;
    template.Properties.CustomRecipes = {
        Configure: layer.recipes.configure
    }

    const securityGroups = layer.securityGroups || [];

    securityGroups.forEach(securityGroup => {
        template.Properties.CustomSecurityGroupIds.push({
            "Fn::GetAtt":[
                securityGroup,
                "GroupId"
            ]
        });
    });

    const instances = { };

    layer.instances.forEach(instance => {
        instances[instance.name] = ec2(layer, instance, stackName);
    });

    const layerObj = { [layer.name]: Object.assign(template, utils.metadata.generate()) };

    return Object.assign(layerObj, instances);
};

module.exports = () => {
    const stack = config.opsWorks;

    if (!stack) {
        return { };
    }

    var layers = { };

    stack.layers.forEach(layer => {
       layers = Object.assign(layers, createLayer(layer, stack.name));
    });

    return layers;
};

   //   "Comment" : "OpsWorks instances require outbound Internet access. Using DependsOn to make sure outbound Internet Access is estlablished before creating instances in this layer."
   //   "DependsOn": [ "NATIPAddress", "PublicRoute", "PublicSubnetRouteTableAssociation", "PrivateRoute", "PrivateSubnetRouteTableAssociation", "OpsWorksApp"]