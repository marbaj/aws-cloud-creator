"user strict"

const utils = require("../utils");
const config = require("../../config");

module.exports = () => {
    const stack = config.opsWorks;

    if (!stack) {
        return { };
    }

    const template = {
        "Type": "AWS::OpsWorks::Stack",
        "Properties": {
            "Name": {
                "Ref": "AWS::StackName"
            },
            "ServiceRoleArn": {
                "Fn::Join": [
                    "",
                    [ "arn:aws:iam::",
                        {
                            "Ref": "AWS::AccountId"
                        },
                        ":role/",
                        {
                            "Ref": "ServiceRole"
                        }
                    ]
                ]
            },
            "DefaultInstanceProfileArn": {
                "Fn::Join": [
                    "",
                    [
                        "arn:aws:iam::",
                        {
                            "Ref": "AWS::AccountId"
                        },
                        ":instance-profile/",
                        {
                            "Ref": "InstanceRole"
                        }
                    ]
                ]
            }
        }
    };

    template.Properties.UseOpsworksSecurityGroups = false;
    template.Properties.UseCustomCookbooks = false;
    
    if (stack.cookbooks === true) {
        template.Properties.UseCustomCookbooks = false;
        template.Properties.CustomCookbooksSource = {
            "Type": "git",
            "Url": stack.chefRepo
        }
    }

    if (config.vpc) {
        template.Properties.VpcId = utils.ref(config.vpc.name);
        template.Properties.DefaultSubnetId = utils.ref(stack.subnet);
    }

    return { [stack.name]: Object.assign(template, utils.metadata.generate()) };
};
