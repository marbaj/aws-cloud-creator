"use strict";

const utils = require("../utils");
const config = require("../../config");

module.exports = (layer, instance, stackName) => {
    const template = {
        Type: "AWS::OpsWorks::Instance",
        Properties: {
            LayerIds: []
        }
    };

    template.Properties.StackId = utils.ref(stackName);
    template.Properties.LayerIds.push(utils.ref(layer.name));
    template.Properties.InstanceType = instance.type;
    template.Properties.SshKeyName = layer.sshKeyName;

    return  Object.assign(template, utils.metadata.generate());
};
