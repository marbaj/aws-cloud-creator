"user strict"

const stack = require("./stack");
const layers = require("./layers");
const elb = require("../elb");

module.exports = () => {
    const opwWorksStack = stack();
    const opsWorksLayers = layers();
    const opsWorksElb = elb();

    return Object.assign(opwWorksStack, opsWorksLayers, opsWorksElb);
};